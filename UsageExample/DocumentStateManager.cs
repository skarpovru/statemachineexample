﻿using System.Collections.Generic;
using StateMachine;

namespace UsageExample
{
    public class DocumentStateManager
    {
        ILocalDatabaseContext DatabaseContext { get; set; }
        IGenericStateManager StateManager { get; set; }

        public DocumentStateManager()
        {
            DatabaseContext = new LocalDatabaseContext();
            IEnumerable<Transition> transitionMatrix = BuildTransitionMatrix();
            //IEnumerable<DocumentStatus> databaseContext = new BuildDatabaseContext();
            StateManager = new GenericStateManager(new DraftDocumentState(DatabaseContext.DocumentStatuses), transitionMatrix);
        }

        public void RunExample()
        {
            StateManager.ActivateTrigger(Triggers.SendForReview);
            StateManager.ActivateTrigger(Triggers.SignReview);
            //StateManager.ActivateTrigger(Triggers.Reject); //Will throw exception
            StateManager.ActivateTrigger(Triggers.SendForApproval);
            StateManager.ActivateTrigger(Triggers.SignApprove);
        }

        private IEnumerable<Transition> BuildTransitionMatrix()
        {
            return new List<Transition>
            {
                new Transition
                {
                    SourceStates = new List<IState>{ new DraftDocumentState(DatabaseContext.DocumentStatuses) },
                    TargetState = new SubmitedForReviewDocumentState(DatabaseContext.DocumentStatuses),
                    Trigger = Triggers.SendForReview
                },
                new Transition
                {
                    SourceStates = new List<IState>{ new SubmitedForReviewDocumentState(DatabaseContext.DocumentStatuses) },
                    TargetState = new ReviewedDocumentState(DatabaseContext.DocumentStatuses),
                    Trigger = Triggers.SignReview
                },
                new Transition
                {
                    SourceStates = new List<IState>{ new ReviewedDocumentState(DatabaseContext.DocumentStatuses) },
                    TargetState = new SubmitedForApprovalDocumentState(DatabaseContext.DocumentStatuses),
                    Trigger = Triggers.SendForApproval
                },
                new Transition
                {
                    SourceStates = new List<IState>{ new SubmitedForApprovalDocumentState(DatabaseContext.DocumentStatuses) },
                    TargetState = new ApprovedDocumentState(DatabaseContext.DocumentStatuses),
                    Trigger = Triggers.SignApprove
                },
                new Transition
                {
                    SourceStates = new List<IState>{ new SubmitedForReviewDocumentState(DatabaseContext.DocumentStatuses), new SubmitedForApprovalDocumentState(DatabaseContext.DocumentStatuses) },
                    TargetState = new DraftDocumentState(DatabaseContext.DocumentStatuses),
                    Trigger = Triggers.Reject
                },
                new Transition
                {
                    SourceStates = new List<IState>{ new DraftDocumentState(DatabaseContext.DocumentStatuses) },
                    TargetState = new DisposedDocumentState(DatabaseContext.DocumentStatuses),
                    Trigger = Triggers.Delete
                },
                new Transition
                {
                    SourceStates = new List<IState>{ new ApprovedDocumentState(DatabaseContext.DocumentStatuses) },
                    TargetState = new ArchivedDocumentState(DatabaseContext.DocumentStatuses),
                    Trigger = Triggers.Archive
                }
            };
        }
    }
}
