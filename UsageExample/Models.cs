﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UsageExample
{
    /// <summary>
    /// Entity to store Document Status in a database
    /// </summary>
    public class DocumentStatus
    {
        public DocumentStatus()
        {
            Active = true;
        }

        /// <summary>
        /// Database primary key
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Status name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Unique identificator to map database record and hardcoded entity
        /// </summary>
        public string Code { get; set; }
        public string Description { get; set; }
        /// <summary>
        /// Active flag
        /// </summary>
        public bool Active { get; set; }

        public ICollection<Document> Documents { get; set; }
    }

    /// <summary>
    /// Entity to store Document details in a database
    /// </summary>
    public class Document
    {
        /// <summary>
        /// Database primary key
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Document name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Unique identificator to map database record and hardcoded entity
        /// </summary>
        public string Code { get; set; }
        public string Description { get; set; }
        public string Body { get; set; }
        /// <summary>
        /// Active flag
        /// </summary>
        public DocumentStatus Status { get; set; }
    }
}
