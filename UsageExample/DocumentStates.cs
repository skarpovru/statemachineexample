﻿using System;
using System.Linq;
using StateMachine;

namespace UsageExample
{
    /// <summary>
    /// Generic document state entity
    /// </summary>
    public class GenericDocumentState : DocumentStatus, IState
    {
        public GenericDocumentState(string code, IQueryable<DocumentStatus> documentStatuses)
        {
            //Search for status record in the database with same code, include only active records
            DocumentStatus documentStatus = documentStatuses.FirstOrDefault(t => t.Code == code && Active);
            if (documentStatus == null)
            {
                throw new ArgumentException("Cannot find any active document status with code \"" + code + "\" in the database");
            }
            Name = documentStatus.Name;
            Code = code;
        }
        public void OnEnter()
        {
            Console.WriteLine("Setting new state " + Name);
        }
        public void OnExit()
        {
            Console.WriteLine("Changing state from " + Name);
        }
    }

    public class DraftDocumentState : GenericDocumentState
    {
        public DraftDocumentState(IQueryable<DocumentStatus> documentStatuses) : base("Draft", documentStatuses) { }
    }
    public class SubmitedForReviewDocumentState : GenericDocumentState
    {
        public SubmitedForReviewDocumentState(IQueryable<DocumentStatus> documentStatuses) : base("SubmitedForReview", documentStatuses) { }
    }
    public class ReviewedDocumentState : GenericDocumentState
    {
        public ReviewedDocumentState(IQueryable<DocumentStatus> documentStatuses) : base("Reviewed", documentStatuses) { }
    }
    public class SubmitedForApprovalDocumentState : GenericDocumentState
    {
        public SubmitedForApprovalDocumentState(IQueryable<DocumentStatus> documentStatuses) : base("SubmitedForApproval", documentStatuses) { }
    }
    public class ApprovedDocumentState : GenericDocumentState
    {
        public ApprovedDocumentState(IQueryable<DocumentStatus> documentStatuses) : base("Approved", documentStatuses) { }
    }
    public class ArchivedDocumentState : GenericDocumentState
    {
        public ArchivedDocumentState(IQueryable<DocumentStatus> documentStatuses) : base("Archived", documentStatuses) { }
    }
    public class DisposedDocumentState : GenericDocumentState
    {
        public DisposedDocumentState(IQueryable<DocumentStatus> documentStatuses) : base("Disposed", documentStatuses) { }
    }  
}
