﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StateMachine;

namespace UsageExample
{
    class Program
    {
        static void Main(string[] args)
        {
            DocumentStateManager stateManager = new DocumentStateManager();
            stateManager.RunExample();
            Console.ReadLine();
        }
    }
}
