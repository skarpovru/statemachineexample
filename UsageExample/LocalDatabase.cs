﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UsageExample
{
    public interface ILocalDatabaseContext
    {
        IQueryable<DocumentStatus> DocumentStatuses { get; set; }
    }

    public class LocalDatabaseContext : ILocalDatabaseContext
    {
        public LocalDatabaseContext()
        {
            BuildDocumentStatuses();
        }
        public IQueryable<DocumentStatus> DocumentStatuses { get; set; }

        private void BuildDocumentStatuses()
        {
            DocumentStatuses = new List<DocumentStatus>
            {
                new DocumentStatus
                {
                    Name = "Draft",
                    Code = "Draft"
                },
                new DocumentStatus
                {
                    Name = "Submited for Review",
                    Code = "SubmitedForReview"
                },
                new DocumentStatus
                {
                    Name = "Reviewed",
                    Code = "Reviewed"
                },
                new DocumentStatus
                {
                    Name = "Submited for Approval",
                    Code = "SubmitedForApproval"
                },
                new DocumentStatus
                {
                    Name = "Approved",
                    Code = "Approved"
                },
                new DocumentStatus
                {
                    Name = "Archived",
                    Code = "Archived"
                },
                new DocumentStatus
                {
                    Name = "Disposed",
                    Code = "Disposed"
                }
            }.AsQueryable();
        }
    }
}
