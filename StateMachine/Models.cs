﻿using System.Collections.Generic;

namespace StateMachine
{
    /// <summary>
    /// A triggering activity that causes a transition to occur
    /// </summary>
    public enum Triggers
    {
        Create,
        SendForReview,
        SignReview,
        SendForApproval,
        SignApprove,
        Reject,
        Archive,
        Delete
    }
    /// <summary>
    /// The basic unit interface that composes a state machine. A state machine can be in one state at any particular time.
    /// </summary>
    public interface IState
    {
        /// <summary>
        /// State name, usually taken from database
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// State unique code for mapping with database entity
        /// </summary>
        string Code { get; set; }
        /// <summary>
        /// Action which should be executed after new state set
        /// </summary>
        void OnEnter();
        /// <summary>
        /// Action which should be executed before old state change
        /// </summary>
        void OnExit();
    }

    /// <summary>
    /// A directed relationship between states which represents the complete response of a state machine to an occurrence of an event of a particular type
    /// </summary>
    public class Transition
    {
        public IEnumerable<IState> SourceStates { get; set; }
        public IState TargetState { get; set; }
        public Triggers Trigger { get; set; }
    }
}
