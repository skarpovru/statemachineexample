﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StateMachine
{
    /// <summary>
    /// State Machine Core 
    /// </summary>
    public interface IGenericStateManager
    {
        IState CurrentState { get; }

        /// <summary>
        /// Process event
        /// </summary>
        void ActivateTrigger(Triggers trigger);
    }

    public class GenericStateManager : IGenericStateManager
    {
        private IEnumerable<Transition> TransitionMatrix { get; set; }
        public IState CurrentState { get; private set; }

        public GenericStateManager(IState defaultState, IEnumerable<Transition> transitionMatrix)
        {
            SetDefaultState(defaultState);
            TransitionMatrix = transitionMatrix;
        }

        private void SetDefaultState(IState defaultState)
        {
            CurrentState = defaultState;
            defaultState.OnEnter();
        }

        public void ActivateTrigger(Triggers trigger)
        {
            //Try to find Transition
            Transition requiredTransition = TransitionMatrix.FirstOrDefault(t => t.Trigger == trigger);
            if (requiredTransition == null)
            {
                throw new ArgumentException("Trigger \"" + trigger + "\" is not found");
            }
            //Chech if transition allowed for current state
            if (!requiredTransition.SourceStates.Select(t => t.Code).Contains(CurrentState.Code))
            {
                throw new NotSupportedException("Requested transition to  state \"" + requiredTransition.TargetState.Name + "\" is not supported for the current state \"" + CurrentState.Name + "\"");
            }

            //Perform actions and change current state
            CurrentState.OnExit();
            CurrentState = requiredTransition.TargetState;
            CurrentState.OnEnter();
        }
    }
}
